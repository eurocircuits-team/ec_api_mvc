﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ECApi.IRepository
{
   public interface IDataAccessRepository
    {
        int ExecuteNonQuery(string strSQLQuery);
        int ExecuteNonQuery(string storeProcedureName, SqlParameter[] parameter);
        int ExecuteNonQuery(string storeProcedureName, object parameter);
        object ExecuteScaler(string strSQLQuery);
        object ExecuteScaler(string strSQLQuery, SqlParameter[] parameter);
        DataSet GetDataSet(string strSQLQuery);
        DataTable GetDataTable(string storeProcedureName, SqlParameter[] parameter, out int totalRecord);
        DataTable GetDataTable(string strSQLQuery, out int totalRecord);
        DataTable GetDataTable(string storeProcedureName, SqlParameter[] parameter);
        DataTable GetDataTable(string strSQLQuery);
        DataSet GetDataSet(string storeProcedureName, SqlParameter[] parameter, int? timeout = default(int?));
        List<T> GetListof<T>(string storeProcedureName, object parameters) where T : class;
        List<T> GetListofWithTotalRecord<T>(string strSQLQuery, out int totalRecord) where T : class;
    }
}
