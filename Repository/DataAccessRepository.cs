﻿using Dapper;
using ECApi.IRepository;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ECApi.Repository
{
    public class DataAccessRepository : IDataAccessRepository
    {
        private string GetConnectionString()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            var root = configurationBuilder.Build();
            return root.GetSection("ConnectionStrings:DefaultConnection").Value;
        }
        public int ExecuteNonQuery(string strSQLQuery)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(GetConnectionString()))
                {
                    sqlConnnection.Open();
                    using (SqlCommand sqlCommand = new SqlCommand(strSQLQuery, sqlConnnection))
                    {
                        sqlCommand.CommandTimeout = 2000;
                        sqlCommand.CommandType = CommandType.Text;
                        return sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }
        public int ExecuteNonQuery(string storeProcedureName, SqlParameter[] parameter)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(GetConnectionString()))
                {
                    sqlConnnection.Open();
                    using (SqlCommand cmd = new SqlCommand(storeProcedureName, sqlConnnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 2000;
                        for (int i = 0; i < parameter.Length; i++)
                        {
                            cmd.Parameters.Add(parameter[i]);
                        }
                        return cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }
        public int ExecuteNonQuery(string storeProcedureName, object parameter)
        {
            try
            {
                using (var connection = new SqlConnection(GetConnectionString()))
                {
                    var result = connection.QuerySingle<int>(storeProcedureName, parameter, commandType: CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }
        public object ExecuteScaler(string strSQLQuery)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(GetConnectionString()))
                {
                    sqlConnnection.Open();
                    using (SqlCommand cmd = new SqlCommand(strSQLQuery, sqlConnnection))
                    {
                        cmd.CommandTimeout = 2000;
                        return cmd.ExecuteScalar();
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }
        public object ExecuteScaler(string strSQLQuery, SqlParameter[] parameter)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(GetConnectionString()))
                {
                    sqlConnnection.Open();
                    using (SqlCommand cmd = new SqlCommand(strSQLQuery, sqlConnnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 2000;
                        for (int i = 0; i < parameter.Length; i++)
                        {
                            cmd.Parameters.Add(parameter[i]);
                        }
                        return cmd.ExecuteScalar();
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }
        public DataSet GetDataSet(string strSQLQuery)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(GetConnectionString()))
                {
                    sqlConnnection.Open();

                    using (SqlCommand cmd = new SqlCommand(strSQLQuery, sqlConnnection))
                    {
                        cmd.CommandTimeout = 2000;
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();
                            adapter.Fill(ds);
                            return ds;
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }
        public DataTable GetDataTable(string storeProcedureName, SqlParameter[] parameter, out int totalRecord)
        {
            try
            {
                totalRecord = 0;
                using (SqlConnection sqlConnnection = new SqlConnection(GetConnectionString()))
                {
                    sqlConnnection.Open();
                    using (SqlCommand cmd = new SqlCommand(storeProcedureName, sqlConnnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 2000;
                        DataTable dt = new DataTable();
                        for (int i = 0; i < parameter.Length; i++)
                        {
                            cmd.Parameters.Add(parameter[i]);
                        }
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();
                            adapter.Fill(ds);
                            if (ds != null && ds.Tables.Count > 0)
                            {
                                dt = ds.Tables[0];
                                totalRecord = Convert.ToInt32(ds.Tables[1].Rows[0]["totalRecord"].ToString());
                            }
                            return dt;
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }
        public DataTable GetDataTable(string strSQLQuery, out int totalRecord)
        {
            try
            {
                totalRecord = 0;
                using (SqlConnection sqlConnnection = new SqlConnection(GetConnectionString()))
                {
                    sqlConnnection.Open();

                    using (SqlCommand cmd = new SqlCommand(SetTransactionIsolationLevel(strSQLQuery), sqlConnnection))
                    {
                        cmd.CommandTimeout = 2000;
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();
                            DataTable dt = new DataTable();
                            adapter.Fill(ds);
                            if (ds != null && ds.Tables.Count > 0)
                            {
                                dt = ds.Tables[0];
                                totalRecord = Convert.ToInt32(ds.Tables[1].Rows[0]["totalRecord"].ToString());
                            }
                            return dt;
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }
        public DataTable GetDataTable(string storeProcedureName, SqlParameter[] parameter)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(GetConnectionString()))
                {
                    sqlConnnection.Open();
                    using (SqlCommand cmd = new SqlCommand(storeProcedureName, sqlConnnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 2000;

                        for (int i = 0; i < parameter.Length; i++)
                        {
                            cmd.Parameters.Add(parameter[i]);
                        }
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();
                            adapter.Fill(ds);
                            return ds.Tables[0];
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }
        public DataTable GetDataTable(string strSQLQuery)
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(GetConnectionString()))
                {
                    sqlConnnection.Open();

                    using (SqlCommand cmd = new SqlCommand(SetTransactionIsolationLevel(strSQLQuery), sqlConnnection))
                    {
                        cmd.CommandTimeout = 2000;
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();
                            DataTable dt = new DataTable();
                            adapter.Fill(ds);
                            if (ds != null && ds.Tables.Count > 0)
                            {
                                dt = ds.Tables[0];
                            }
                            return dt;
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }
        public DataSet GetDataSet(string storeProcedureName, SqlParameter[] parameter, int? timeout = default(int?))
        {
            try
            {
                using (SqlConnection sqlConnnection = new SqlConnection(GetConnectionString()))
                {
                    sqlConnnection.Open();

                    using (SqlCommand cmd = new SqlCommand(storeProcedureName, sqlConnnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 2000;
                        for (int i = 0; i < parameter.Length; i++)
                        {
                            cmd.Parameters.Add(parameter[i]);
                        }
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            DataSet ds = new DataSet();

                            if (timeout.HasValue)
                            {
                                adapter.SelectCommand.CommandTimeout = timeout.Value;
                            }
                            adapter.Fill(ds);
                            return ds;
                        }
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }
        // for return data as list 
        public List<T> GetListof<T>(string storeProcedureName, object parameters) where T : class
        {
            try
            {
                DefaultTypeMap.MatchNamesWithUnderscores = true;
                using (IDbConnection sqlConnnection = new SqlConnection(GetConnectionString()))
                {
                    return sqlConnnection.Query<T>(storeProcedureName, parameters, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<T> GetListofWithTotalRecord<T>(string strSQLQuery, out int totalRecord) where T : class
        {
            try
            {
                totalRecord = 0;
                List<T> items = new List<T>();
                DefaultTypeMap.MatchNamesWithUnderscores = true;
                using (IDbConnection db = new SqlConnection(GetConnectionString()))
                {
                    using (var result = db.QueryMultiple(strSQLQuery, null))
                    {
                        items = result.Read<T>().ToList();
                        // TODO for total recourd count 
                        var totalRecords = result.Read<dynamic>().ToList();
                        totalRecord = totalRecords[0].totalRecord;
                    }
                }
                return items;
            }
            catch (SqlException)
            {
                throw;
            }
        }
        private static string SetTransactionIsolationLevel(string inputString)
        {
            //set transaction isolation level read uncommitted
            if (inputString.ToLower().StartsWith("select"))
            {
                inputString = "set transaction isolation level read uncommitted ;" + inputString;
            }
            return inputString;
        }
    }
}
