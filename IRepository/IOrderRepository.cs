﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ECApi.IRepository
{
    public interface IOrderRepository
    {
        DataTable GetOrder(string SeachData);
        public string SearchOrder(dynamic SearchData);
    }
}
