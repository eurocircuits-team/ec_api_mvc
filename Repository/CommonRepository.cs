﻿using ECApi.IRepository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECApi.Repository
{
    public class CommonRepository: ICommonRepository
    {
        private IDataAccessRepository _dataAccess;
        public CommonRepository() {
            _dataAccess = new DataAccessRepository();
        }
        public virtual string GetGridData(string seachData, string spName, List<SqlParam> whereBlockList = null, SqlParameter[] sqlParameters = null)
        {
            int startRec = 0;
            int draw = 1;
            int pageSize = 10;
            int sortCol = 1;
            string sortOrder = "asc";
            dynamic sPostData = JArray.Parse(seachData) as JArray;
            List<SqlParam> objParaList = new List<SqlParam>();
            dynamic columns = null;
            foreach (dynamic data in sPostData)
            {
                if (data.name != null)
                {
                    if (data.name == "order")
                    {
                        foreach (dynamic jobj in data.value)
                        {
                            sortCol = Convert.ToInt32(jobj.column);
                            sortOrder = jobj.dir;
                            break;
                        }
                    }
                    else if (data.name == "start")
                    {
                        startRec = Convert.ToInt32(data.value);
                    }
                    else if (data.name == "draw")
                    {
                        draw = Convert.ToInt32(data.value);
                    }
                    else if (data.name == "length")
                    {
                        pageSize = Convert.ToInt32(data.value);
                    }
                    else if (data.name == "columns")
                    {
                        columns = data.value;
                    }
                    else if (data.name != "order" && data.name != "search" && data.name != "columns" && data.name != "start" && data.name != "length" && data.name != "draw")
                    {
                        SqlParam objPara = new SqlParam();
                        objPara.Name = data.name;
                        objPara.Value = data.value;
                        objParaList.Add(objPara);
                    }
                }
            }
            string sortBy = "id";
            if (columns != null)
            {
                sortBy = ((Newtonsoft.Json.Linq.JContainer)(((Newtonsoft.Json.Linq.JContainer)(columns[sortCol])).First)).First.ToString();
                if (sortBy == "")
                {
                    sortBy = "id";
                }
            }

            if (whereBlockList != null)
            {
                objParaList.AddRange(whereBlockList);
            }

            string where = " 1=1 ";
            foreach (SqlParam objPara in objParaList)
            {
                if (objPara.Value != "")
                {
                    where = where + " and " + objPara.Name + " like '%" + objPara.Value + "%'";
                }
            }

            SqlParameter[] sqlParams = new SqlParameter[]
             {
                 new SqlParameter("@startRec", startRec),
                 new SqlParameter("@pageSize", pageSize),
                 new SqlParameter("@SortBy", sortBy),
                 new SqlParameter("@SortDir", sortOrder),
                 new SqlParameter("@WhereClause", where),
             };

            if (sqlParameters != null)
            {
                sqlParams = sqlParams.Concat(sqlParameters).ToArray();
            }
            int totalRecord = 0;
            DataTable dataTable = _dataAccess.GetDataTable(spName, sqlParams, out totalRecord);
            var sb = new StringBuilder();
            sb.Append("{");
            sb.Append(@"""recordsTotal"": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append(@"""recordsFiltered"": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append(@"""draw"": ");
            sb.Append(draw);
            sb.Append(", ");
            sb.Append(@"""data"":");
            sb.Append(JsonConvert.SerializeObject(dataTable));
            sb.Append("}");
            return sb.ToString(); ;
        }
    }
    
}
