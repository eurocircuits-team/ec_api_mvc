﻿namespace ECApi.Repository
{
    public class SqlParam
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}