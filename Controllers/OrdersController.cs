﻿using ECApi.Filters;
using ECApi.IRepository;
using ECApi.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ECApi.Controllers
{
    [ApiController]
    [AuthenticationAttribute("basic")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderRepository _orderRepository;
        public OrdersController(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }
        [HttpPost, HttpGet]
        [Route("api/searchorders")]
        public string SearchOrder([FromBody] dynamic SearchData)
        {
            dynamic requestData = JsonConvert.DeserializeObject(SearchData.ToString());
            return _orderRepository.SearchOrder(requestData);
        }
    }
}
