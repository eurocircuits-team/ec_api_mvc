﻿using ECApi.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ECApi.IRepository
{
    public interface ICommonRepository
    {
        string GetGridData(string seachData, string spName, List<SqlParam> whereBlockList = null, SqlParameter[] sqlParameters = null);
    }
}
