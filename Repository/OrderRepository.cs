﻿using ECApi.IRepository;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ECApi.Repository
{
    public class OrderRepository : CommonRepository,IOrderRepository
    {
        private IDataAccessRepository _dataAccess;
        public OrderRepository() {
            _dataAccess= new DataAccessRepository(); 
        }
        DataTable IOrderRepository.GetOrder(string SeachData)
        {
            throw new NotImplementedException();
        }
        public string SearchOrderData(FormCollection collection) {
            string requestId = collection["requestId"].ToString();
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@codeid", requestId)
            };
            string postData = collection["postdata"].ToString();
            return GetGridData(postData, "sp_searchorders", null, sqlParams);
        }
        public string SearchOrder(dynamic SearchData)
        {
            try
            {
                string whereCluase = " 1=1";
                string rowLimit = "100";
                foreach (var item in SearchData)
                {
                    if (item.Name.ToString() == "RowLimit" && item.Value.ToString() != "")
                    {
                        rowLimit = item.Value.ToString();
                    }
                    else if (item.Value.ToString() != "" && item.Name.ToString() != "OrderStartDate" && item.Name.ToString() != "OrderEndDate" && item.Name.ToString() != "FromDeliveryDate" && item.Name.ToString() != "TillDeliveryDate")
                    {
                        whereCluase += " and ";
                        whereCluase += item.Name + " like \"" + item.Value.ToString() + "%\"";
                    }
                }
                if (SearchData.OrderStartDate != null && SearchData.OrderEndDate != null && SearchData.OrderStartDate.ToString() != "" && SearchData.OrderEndDate.ToString() != "")
                {
                    whereCluase += " and CONVERT(date,so.OrderDate, 103) between CONVERT(datetime,'" + SearchData.OrderStartDate.ToString() + "',103) and  CONVERT(datetime,'" + SearchData.OrderEndDate.ToString() + "',103)";
                }
                if (SearchData.FromDeliveryDate != null && SearchData.TillDeliveryDate != null && SearchData.FromDeliveryDate.ToString() != "" && SearchData.TillDeliveryDate.ToString() != "")
                {
                    whereCluase += " and CONVERT(date,so.DeliveryDate, 103) between CONVERT(datetime,'" + SearchData.FromDeliveryDate.ToString() + "',103) and  CONVERT(datetime,'" + SearchData.TillDeliveryDate.ToString() + "',103)";
                }
                whereCluase = whereCluase.Replace("\"", "'");
                DataTable dataTable = new DataTable();
                dataTable = _dataAccess.GetDataTable(string.Format("exec sp_searchorders '{0}','{1}'", whereCluase.Replace("'", "''"), rowLimit));
                if (dataTable.Rows.Count > 0)
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(new { code = "1", data = dataTable, message = "" });
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(new { code = "2", data = "", message = "No record(s) found" });
                }
            }
            catch (Exception ex)
            {
                var obj = new
                {
                    code = "0",
                    message = "Internal Server Error." + ex.ToString()
                };
                return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            }
        }

    }
}
